<?php ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>register</title>
    <link rel="stylesheet" type="text/css" href="registration.css">
    <img src="logo.jpg" width="550" height="120">
    <style>
        table{
            background-color: #1ebb90;
            align-items: center;
            padding-left: 100px;
            padding-right: 100px;
            width: 200px;
            height: 500px;
            padding-top: 20px;
            opacity: 0.8;
        }
        table input[type=text]{
            padding: 7px;
            border:5px solid black;
            font-size: 16px;
            font-family: 'Arial', sans-serif;
            width: 500px;
            border-spacing:20px ;
        }
        table tr td{
            width: 250px;
        }
        table input[type=password]{
            padding: 7px;
            border:5px solid black;
            font-size: 16px;
            font-family: 'Arial', sans-serif;
            width: 500px;
            border-spacing:20px ;
        }
        table input[type=submit]{
            float: right;
            background: orange;
            border-radius: 0 5px 5px 0;
            cursor:pointer;
            position: relative;
            padding: 7px;
            font-family: 'Arial', sans-serif;
            border: none;
            font-size: 16px;
            width: 170px;
            border: 5px solid black;
        }
        table  h2{
            font-size: large;
            border: 5px solid deeppink;
            background-color:black;
            color: white;
            width: 200px;
        }
        table h2:hover{
            background-color: white;
            color: deeppink;
        }
        #gu{
            color: white;
        }

    </style>

    <script type="text/javascript">
        function formValidate() {
            var Name = document.forms["register1"]["fullName"].value;
            var Contact_Number = document.forms["register1"]["contact"].value;
            var Email = document.forms["register1"]["email"].value;
            var User_Name = document.forms["register1"]["uName"].value;
            var Password = document.forms["register1"]["pw"].value;
            var Reenter_Password = document.forms["register1"]["pw2"].value;

            if (isAlphabetic(Name))
                if (isNumeris(Contact_Number))
                    if (emailValidation(Email))
                        if (userValidation(User_Name))
                            if (passwordValidation(Password)){
                                alert("Registration Successful");
                                return true;}
                            else
                                return false;
                        else
                            return false;
                    else
                        return false;
                else
                    return false;
            else
                return false;
        }





        function isEmpty(elemValue, field) {
            if (elemValue == "" || elemValue == null) {
                alert("You cannot have " + field + " filled empty");
                return true;
            }
            else
                return false;
        }

        function isAlphabetic(elemValue) {
            var exp = /^[a-zA-Z]+$/;
            if (!isEmpty(elemValue, "Name")) {
                if (elemValue.match(exp)) {
                    return true;
                }
                else {
                    alert("Enter only Alphabet characters");
                    return false;
                }
            }
            else
                return false;
        }

        function isNumeris(elemValue) {
            if (!isEmpty(elemValue, "Contact Number")) {
                var exp = /^[0-9]+$/;
                if (elemValue.match(exp)) {
                    return true;
                }
                else {
                    alert("Enter a valid contact number");
                    return false;
                }
            }
            else
                return false;


        }
        function userValidation(elemValue, min, max) {
            min = "6";
            max = "8";
            if (!isEmpty(elemValue, "User Name")) {
                if (elemValue.length >= min && elemValue.length <= max) {
                    return true;
                }
                else {
                    alert("Enter a User Name in between " + min + " and " + max + " characters");
                    return false;
                }

            }
            else
                return false;
        }

        function emailValidation(elemValue) {

            if (!isEmpty(elemValue, "Email")) {
                var atSymbol = elemValue.indexOf("@");
                var dotSymbol = elemValue.indexOf(".");

                if (atSymbol < 1 || dotSymbol + 2 >= elemValue.length || atSymbol + 2 > dotSymbol) {
                    alert("Enter a valid email address");
                    return false;
                }
                else
                    return true;
            }
            else
                return false;
        }

        function passwordValidation(elemValue) {
            var exp= /^[0-9a-zA-Z]+$/;
            if(!isEmpty(elemValue, "Password"))
            {
                if (elemValue.match(exp))
                {
                    return true;
                }
                else{
                    alert("Incorrect password. Please try again!");
                    return false;
                }
            }
            else
                return false;

        }


    </script>
</head>
<body>
<div class="header">
    <div class="topHeader">
        <div class="logo">
            <img src="e-medic%20logo1.jpg" width="215" height="200">
        </div> <!-- end logo -->
        <p>
            <a href="login1.php"style="margin-top: 50px"><b>Login</b></a> |
            <a href="register1.php" style="margin-top: 50px"><b>Register</b></a>
        </p>
        <p>
            <br /> <h1 style="color: black; font-size:50px; padding-right: 380px;">E-REGISTRATION</h1>
        </p>
    </div>
    <ul>
        <li><a href="project.php">Home</a> </li>
        <li class="dropdown"><a href="#" class="dropdown-btn">Products</a>
            <div class="dropdown-menu">
                <a href="non-clinical.php">Non-clinical Items</a>
                <a href="Clinical.php">Clinical Items</a>
                <a href="drugs.php">Drugs</a>
            </div>
        </li>
        <li><a href="about%20us.php">About Us</a> </li>
        <li><a href="com.php">Help & Contact</a> </li>
        <li><a href="policies.php">Policies</a> </li>
    </ul>
    <form class="search-form">
        <input type="text" placeholder="Search for anything">
        <button>Search</button>
    </form>
</div>
        <form id="register1" onsubmit = "return formValidate()" form action="?php echo $SERVER['PHP_SELF'];?>" method="post">
            <br />
            <br />
            <br />
            <br />
            <br />


            <table align="center">
                <tr><h1 align="center">REGISTERTION</h1>

                    <td>Name </td>&nbsp;
                    <td><input id="Text" type="text" name="fullName" />
                    </td>
                </tr>

                <tr>
                    <td>Contact Number  </td>
                    <td><input id = "Text" type="text" name = "contact"/></td>
                </tr>

                <tr>
                    <td> Email  </td>
                    <td><input id="Text" type="text" name="email" /> </td>
                </tr>
                <tr>
                    <td>
                        User Name  </td>

                    <td><input id="Text" type = "text" name = "uName"/></td>
                </tr>
                <tr>

                    <td> Password  </td>
                    <td><input id="Text" type="password"   placeholder="Enter password" name="pw"/></td>

                </tr>

                <tr><td>Re-Enter password </td>
                    <td><input id="Text" type="password" name="pw2" placeholder="Confirm Password"/>
                    </td></tr>

                <tr><td><input type="submit" onsubmit="formValidate()" value="Register"></td>
                </tr>
                <tr>
                    <td><h2>Already have a account <a href="login1.php">Login</a></h2></td>
                    <td><h1> OR <a href="check%20payment.php">GUEST USER > ></a></h1></td>
                </tr>
            </table>
        </form>


    </div>
<br />
<br />
<br />
<br />
<br />
<br />
<footer>    <b> Copyright @ E- MEDIC, 2017 - All rights Reserved</b>
</footer> <!-- end footer -->


</body>
</html>


<?php

$con=mysqli_connect("localhost","root","sliit","Emedic");

if(mysqli_connect_errno()){
    echo"Failed to connect".mysqli_connect_errno();

}

$fullName = $_POST['fullName'];
$contact= $_POST['contact'];
$email = $_POST['email'];
$uName = $_POST['uName'];
$pw = $_POST['pw'];
$pw2=$_POST['pw2'];


$sql="INSERT INTO `regi`(`id`, `fullName`, `contact`, `email`, `uName`, `pw`) VALUES('$id','$fullName','$contact','$email', '$uName', '$pw')";

if(!mysqli_query($con,$sql)){
    die('Error:'.mysqli_error($con));
}
echo"1 record added";

mysqli_close($con);

?>