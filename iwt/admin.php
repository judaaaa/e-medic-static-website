<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="registration.css">

    <style>
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        img{
            opacity: 0.8;
        }
        td {
            padding-bottom: 15px;
            padding-top: 15px;
            padding-left: 160px;
            padding-right: 15px;
        }

        table h1{
            text-align: center;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>

<div class="header">
    <div class="topHeader">
        <div class="logo">
            <img src="e-medic%20logo1.jpg" width="215" height="200">
        </div> <!-- end logo -->
        <p>
            <a href="login1.php"style="margin-top: 50px"><b>Login</b></a> |
            <a href="register1.php" style="margin-top: 50px"><b>Register</b></a>
        </p>
        <p>
            <br /> <h1 style="color: black; font-size:50px; padding-right: 380px;">E-REGISTRATION</h1>
        </p>
    </div>
    <ul>
        <li><a href="project.php">Home</a> </li>
        <li class="dropdown"><a href="#" class="dropdown-btn">Products</a>
            <div class="dropdown-menu">
                <a href="non-clinical.php">Non-clinical Items</a>
                <a href="Clinical.php">Clinical Items</a>
                <a href="drugs.php">Drugs</a>
            </div>
        </li>
        <li><a href="about%20us.php">About Us</a> </li>
        <li><a href="com.php">Help & Contact</a> </li>
        <li><a href="policies.php">Policies</a> </li>
    </ul>
    <form class="search-form">
        <input type="text" placeholder="Search for anything">
        <button>Search</button>
    </form>

<table width="100%" >
    <tr height="50%">
        <td align="center" bgcolor="#7fff00" >
            <img src="visible.png" style="width:150px;height:110px;">
            <h2>Visitors</h2>
            <h1>14,300</h1>
        </td>
        <td bgcolor="#5f9ea0" align="center">
            <img src="user.png" style="width:150px;height:110px;">
            <h2>Users</h2>
            <h1>1,750</h1>
        </td>
        <td bgcolor="#deb887" align="center">
            <img src="download.png" style="width:150px;height:110px;">
            <h2>Sales</h2>
            <h1>3,500</h1>
        </td>
        <td bgcolor="aqua" align="center">
            <img src="cart.png" style="width:150px;height:110px;">
            <h2>Orders</h2>
            <h1>2,500</h1>
        </td>
    </tr>
</table>
<br/>

<table width="100%" >
    <tr height="50%">
        <td bgcolor="#faebd7">
            <h1>VISITORS STATISTICS</h1>
            <img src="html.jpg" style="width:920px;height:420px">
        </td>
    </tr>
</table>

<table width="50">
    <tr height="30%">
        <td bgcolor="#f0f8ff">
            <h2>CALENDER WIDGET</h2>
            <img src="Calendar.jpg" style="width: 500px;height: 500px">
        </td>
        <td bgcolor="#f0f8ff">
            <img src="notify.JPG" style="width: 500px;height: 500px">
        </td>
    </tr>
</table>

<table width="100%" >
    <tr height="30%">
        <td bgcolor="#faebd7">
            <img src="month.PNG" style="width:250px;height:320px;">
        </td>
        <td bgcolor="#faebd7">
            <img src="daily.JPG" style="width:250px;height:320px;">
        </td>
        <td bgcolor="#faebd7">
            <img src="yearly.png" style="width:250px;height:320px;">
        </td>
    </tr>
</table>
<table width="100">
    <tr height="50%">
        <td bgcolor="#f0f8ff">
            <img src="item.PNG" style="width: 900px;height: 300px">
        </td>
    </tr>
</table>